﻿#include <iostream>
 class MyClass
{
public:
	MyClass() : a(10), b(5)
	{}
	MyClass(int _a, int _b): a(_a), b(_b)
	{}

	void Show(int a, int b) 
	{
		std::cout << a << " " << b;
	}
private:
	int a;
	int b;
	//int c;
};
 class Vector
 {
 public:
	 
	 Vector(): x(), y(), z()
	 {}
	 Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
	 {}
	 double Module(double x, double y, double z)
	 {
		 double Mod = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
		 std::cout << Mod;
		 return Mod;
	 }
	 
	 void Pokaz(double x, double y, double z)
	 {
	 std::cout << "\n" << x << " " << y << " " << z << "\n";
	 
	 }

 private:
	 double x;
	 double y;
	 double z;
 };

int main()
{
	MyClass m;
	m.Show(5,12);
	Vector v;
	v.Pokaz(9,6,2);
	v.Module(9,6,2);
}
